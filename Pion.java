package jeu;

public class Pion {
	final int id; // Id du pion

	final boolean blanc; //le pion est il blanc?
	final boolean grand; //le pion est il grand?
	final boolean rond; //le pion est il rond?
	final boolean point; //le pion a t'il un point?
	
	//i => [1-16]
	public Pion(int i){
		id = i;
		blanc = i<=8; // A expliquer de vive voix...
		grand = i%4==0 || i%4==3; // A expliquer de vive voix...
		rond = i%8>=1 && i%8<=4; // A expliquer de vive voix...
		point = i%2==0; // A expliquer de vive voix...
		
	}
	
	//Affichage du pion (affiche "X" si id =0 et affiche l'id sinon
	public void affichePion(){
		if (id == 0){
			System.out.print("X ");
		}else{
		System.out.print(id +" ");
		}
	}

	// Getter de l'id
	public int getId() {
		return id;
	}
	
}