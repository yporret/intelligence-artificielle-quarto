package jeu;
/**
 * @author yann
 *
 */
public class Plateau {
	
	private Pion[] reserve; // Tableau avec les pions non joués
	private Pion[][] jeu; // tableau 4*4 représentant le plateau
	private Pion aJouer; // Pion séléctionné par le joueur précédant
	
	
	// initialisation du jeu
	public Plateau(){
		reserve = new Pion[16]; //création de la reserve
		for(int i = 0; i<=15; i++){
			reserve[i]= new Pion(i+1); //Remplissage de la réserve
		}
		
		jeu = new Pion[4][4]; // initialisation du plateau
		
		
	}
	
	// Affichage de la Réserve
	public void afficheRes(){
		System.out.println(); //saut de ligne pour faire jolie
		for (int i = 0; i<=15; i++){ //affichage de 4 pion par ligne
			System.out.println();
			reserve[i++].affichePion();
			reserve[i++].affichePion();
			reserve[i++].affichePion();
			reserve[i].affichePion();
			
		}
	}
	
	// Affichage du Plateau
	public void afficheJeu(){
		System.out.println();
		for (int i = 0; i<= 3; i++){
			System.out.println();
			for(int j = 0; j<= 3; j++){
				if(jeu[i][j] == null){ //case vide représentée par "X"

					System.out.print("X ");
				}else{
				jeu[i][j].affichePion(); //affichage du pion
				}
			}	
		}
		
	}

	// Retire le Pion p de la réserve et refresh la réserve
	// TODO => Prévoir cas impossible?
	public void oter(Pion p){
		reserve[p.getId() -1] = new Pion(0); // remplace le Pion de la réserve par un pion d'id 0 (bof...)
		afficheRes();
	}

	// séléctionne le Pion p
	// TODO => Prévoir cas Pion déja joué
	public void choisir(Pion p){
		aJouer = p;
		oter(p);
	}
	
	//Place le pion aJouer en x,y (0-3)
	public void placer(int x,int y){
		if (jeu[x][y] == null){ //si case libre
			jeu[x][y] = aJouer;
			aJouer = null;
			afficheJeu(); //refresh le plateau
		}
	}
}